import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { HttpInterceptor } from '@angular/common/http/src/interceptor';
import { Observable } from 'rxjs';
import { map, finalize, tap , catchError } from 'rxjs/operators';
// import 'rxjs/add/operator/finally';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/do';

@Injectable()
export class HttpService {
    constructor(
        private http: HttpClient,
    ) { }


    Post<T>(url: string, body: any, options = {}) {
        // console.log(url, body)
        // this.spinnerService.show();
        console.log('start')
        return this.http.post(url, body, options).pipe(
            map(res => {
                return res['data'];
            }),finalize(() => { 
                console.log('final')
                // this.spinnerService.hide() 
            }),catchError(err => 'asasasasasaas')
        )
    }

    Get<T>(url: string, params?: any) {
        // this.spinnerService.show();
        console.log('start')
        return this.http.get<T>(url, { params: params }).pipe(
            map(res => {
                return res ? res : [];
            }),finalize(() => { 
                // this.spinnerService.hide() 
                console.log('final')
            })
        )
    }

    // Post<T>(url: string, body: any) {
    //     // this.spinner.show();
    //     return this.http.post(url, body).map(res => {
    //         return res['data'] ? res['data'] : []
    //     }).catch(err => {
    //         return Observable.throw(err)
    //     }).finally(() => { });
    // }

    // Delete<T>(url: string, params?: any) {
    //     this.spinner.show();
    //     return this.http.delete(url, { params: params }).catch(err => {
    //         return Observable.throw(err)
    //     }).finally(() => { this.spinner.hide() });
    // }
}

// @Injectable()
// export class AuthHttpService implements HttpInterceptor {

//     constructor(
//         // private authService: AuthService
//     ) { }

//     intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         req = req.clone({
//             setHeaders: {
//                 Authorization: `Bearer demo`
//             },
//         });
//         return next.handle(req);
//     }
// }

@Injectable()
export class HttpErrorService implements HttpInterceptor {

    constructor(
        // private toastService: ToastNotificationService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            tap((event: HttpEvent<any>) => {}, (err: any) => {
                // if (err instanceof HttpErrorResponse) {
                  
                // }
                if (err instanceof Error) {
                    console.error('An error accured', err.message);
                } else {
                    switch (err.status) {
                        case 0:
                            console.log('Rất xin lỗi!!!Server đang bảo trì')
                            // this.toastService.error('Rất xin lỗi!!!Server đang bảo trì');
                            break;
                        case 404:
                            console.log(err['statusText'])
                            // this.toastService.error('Rất xin lỗi!!!Server đang bảo trì');
                            break;
                        default:
                            console.log(err['error'])
                            // this.toastService.error(err.error['error']);
                            break;
                    }
                }
              })
        )
    }
}