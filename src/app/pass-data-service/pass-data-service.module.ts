import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PassDataServiceRoutingModule } from './pass-data-service-routing.module';
import { PassDataServiceComponent } from './pass-data-service.component';
import { OutComponent } from './out/out.component';
import { InComponent } from './in/in.component';

@NgModule({
  declarations: [PassDataServiceComponent, OutComponent, InComponent],
  imports: [
    CommonModule,
    PassDataServiceRoutingModule
  ]
})
export class PassDataServiceModule { }
