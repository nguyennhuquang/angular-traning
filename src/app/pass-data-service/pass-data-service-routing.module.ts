import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PassDataServiceComponent } from './pass-data-service.component';
import { OutComponent } from './out/out.component';
import { InComponent } from './in/in.component';

const routes: Routes = [
  {path:'', children:[
      {path:'out', component:OutComponent},
      {path:'in', component:InComponent}
  ], component:PassDataServiceComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PassDataServiceRoutingModule { }
