import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Post } from '../model/posts.model';

@Injectable({
  providedIn: 'root'
})
export class SenddatapostService {
  dataPost = [] as Post[]
  private dataPostSeed = new BehaviorSubject<Post[]>(this.dataPost);
  currentData = this.dataPostSeed.asObservable();

  constructor() { }

  changeData(data:Post[]){
    this.dataPostSeed.next(data)
  }
}
