import { Component, OnInit } from '@angular/core';
import { SenddatapostService } from '../senddatapost.service';
import { Post } from 'src/app/model/posts.model';

@Component({
  selector: 'app-in',
  templateUrl: './in.component.html',
  styleUrls: ['./in.component.scss']
})
export class InComponent implements OnInit {
  dataSeed = [] as Post []
  constructor(
    private  dataSeedService : SenddatapostService 
  ) { }

  ngOnInit() {
    this.dataSeedService.currentData.subscribe(
        data =>{
          this.dataSeed = data
          console.log(this.dataSeed)
        }
    )
  }

}
