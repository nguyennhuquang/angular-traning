import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { Router } from '@angular/router';
import { Post } from 'src/app/model/posts.model';
import { SenddatapostService } from '../senddatapost.service';

@Component({
  selector: 'app-out',
  templateUrl: './out.component.html',
  styleUrls: ['./out.component.scss']
})
export class OutComponent implements OnInit {
  dataPosts = [] as Post[] 
  constructor(
    private apiService      : ApiService,
    private router          : Router,
    private seedDataService : SenddatapostService
  ) { }

  ngOnInit() {
    this.apiService.getPosts().subscribe(
      res => {
        this.dataPosts = res as Post[]
        // console.log(this.dataPosts)
      }
    )
  }

  onSendDataPost(){
    this.seedDataService.changeData(this.dataPosts)
    this.router.navigate(['./pass/in'])
  }

}
