import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms'
import { from } from 'rxjs';
import { HttpService, HttpErrorService } from 'src/http/http.service';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
      HttpService,
    {
        provide: HTTP_INTERCEPTORS,
        useClass: HttpErrorService,
        multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
