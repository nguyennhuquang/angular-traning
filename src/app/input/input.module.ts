import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputRoutingModule } from './input-routing.module';
import { InputComponent } from './input.component';
import { ChildComponent } from './child/child.component';
import { ParentComponent } from './parent/parent.component';
import { from } from 'rxjs';
import { Child2Component } from './child2/child2.component';

@NgModule({
  declarations: [InputComponent, ChildComponent, ParentComponent, Child2Component],
  imports: [
    CommonModule,
    InputRoutingModule,
  ]
})
export class InputModule { }
