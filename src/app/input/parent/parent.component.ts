import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { Users } from 'src/app/model/users.model';
import { Router } from '@angular/router';
import { SendDataService } from 'src/app/service/send-data.service';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {
  message: string = 'Xin chào, ParentComponent'
  dataMess:string
  userInfo: Users[] 
  dataShare: Users
  dataSeedService = <Users>{}
  printable :boolean = true
  constructor(
    private api : ApiService,
    private router : Router,
    private dataSend : SendDataService
  ) { }

  ngOnInit() {
    this.dataSend.currentData.subscribe(data => this.dataSeedService = data)
    this.dataSend.currentMessage.subscribe(data => this.dataMess = data)

    this.api.getUserInfo().subscribe(
      res => {
        this.userInfo = res
        this.userInfo.forEach(element => {
          element.avatar = 'https://mesa-english.s3-ap-northeast-1.amazonaws.com/word/image/1564979536023-good_morning.jpg'
        });
        this.dataShare= this.userInfo[0]
      }
    )
  }

  onSelected(data:Users){
    this.dataShare = data
  }
  goToOutPut(){
    this.router.navigate(['/output/parent'])
    this.dataSend.changeMessage('Hello from Sibling');
    this.dataSend.changeObj(this.userInfo[0]);
  }
}
