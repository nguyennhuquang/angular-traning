import { Component, OnInit, Input } from '@angular/core';
import { Users } from 'src/app/model/users.model';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
  @Input() dataUser : Users;
  _avatarUrl: string = '';

  constructor() { }

  ngOnInit() {
  }
}
