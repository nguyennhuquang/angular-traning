import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ParentComponent } from './parent/parent.component';
import { InputComponent } from './input.component';

const routes: Routes = [
  {path:'', children:[
    {path:'parent', component:ParentComponent}
  ], component:InputComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InputRoutingModule { }
