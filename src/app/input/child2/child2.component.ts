import { Component, OnInit, Input } from '@angular/core';
import { Users } from 'src/app/model/users.model';

@Component({
  selector: 'app-child2',
  templateUrl: './child2.component.html',
  styleUrls: ['./child2.component.scss']
})
export class Child2Component implements OnInit {
  @Input() data : Users
  constructor() { }

  ngOnInit() {
  }

}
