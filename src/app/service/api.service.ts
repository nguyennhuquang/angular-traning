import { Injectable } from '@angular/core';
import { API_URL } from '../common/api';
import { Post } from '../model/posts.model';
import { HttpService } from 'src/http/http.service';
import { Users } from '../model/users.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private httpService : HttpService
  ) { }

  getUserInfo(){
    return this.httpService.Get<Users[]>(API_URL.getUsers)
  }
  getPosts(){
    return this.httpService.Get<Post[]>(API_URL.getPost)
  }
}
