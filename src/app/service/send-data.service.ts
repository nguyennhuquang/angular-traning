import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Users } from '../model/users.model';

@Injectable({
  providedIn: 'root'
})
export class SendDataService {
  messageSource = new BehaviorSubject<string>("default message");
  currentMessage = this.messageSource.asObservable();

  dataUser = <Users>{}
  dataSending = new BehaviorSubject<Users>(this.dataUser);
  currentData = this.dataSending.asObservable();
  constructor() { }

  changeMessage(message) {
    this.messageSource.next(message);
  }

  changeObj(data:Users) {
    this.dataSending.next(data);
  }
}
