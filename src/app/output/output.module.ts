import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OutputRoutingModule } from './output-routing.module';
import { OutputComponent } from './output.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';

@NgModule({
  declarations: [OutputComponent, ParentComponent, ChildComponent],
  imports: [
    CommonModule,
    OutputRoutingModule
  ]
})
export class OutputModule { }
