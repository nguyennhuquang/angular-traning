import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss']
})
export class ChildComponent implements OnInit {
  @Output() voteSize = new EventEmitter();
  counter: number = 0;
  constructor() { }

  ngOnInit() {
  }

  voted(){
    this.counter ++;
    this.voteSize.emit(this.counter);
  }
}
