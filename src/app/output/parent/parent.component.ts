import { Component, OnInit } from '@angular/core';
import { SendDataService } from 'src/app/service/send-data.service';
import { Users } from 'src/app/model/users.model';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.scss']
})
export class ParentComponent implements OnInit {
  vote: number = 0;
  dataMess:string
  dataSeedService = <Users>{} 
  constructor(
    private dataSend : SendDataService
  ) { }

  ngOnInit() {
    this.dataSend.currentMessage.subscribe(message => this.dataMess = message)
    this.dataSend.currentData.subscribe(data => this.dataSeedService = data)
    console.log(this.dataSeedService)
  }

  voteCount(value:number) {
    this.vote = value;
  }

}
