import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OutputComponent } from './output.component';
import { ParentComponent } from './parent/parent.component';

const routes: Routes = [
  {path:'', children:[
    {path:'parent', component:ParentComponent}
  ], component:OutputComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OutputRoutingModule { }
