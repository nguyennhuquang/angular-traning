import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpRoutingModule } from './http-routing.module';
import { HttpComponent } from './http.component';
import { CallApiComponent } from './call-api/call-api.component';
import { HttpService } from 'src/http/http.service';

@NgModule({
  declarations: [HttpComponent, CallApiComponent],
  imports: [
    CommonModule,
    HttpRoutingModule
  ]
})
export class HttpModule { }
