import { Component, OnInit } from '@angular/core';
import { API_URL } from 'src/app/common/api';
import { Post } from 'src/app/model/posts.model';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-call-api',
  templateUrl: './call-api.component.html',
  styleUrls: ['./call-api.component.scss']
})
export class CallApiComponent implements OnInit {
  data: Post[]
  constructor(
    private http : ApiService
  ) { }

  ngOnInit() {

    this.http.getPosts().subscribe(
      res=> {
        this.data = res
        console.log(res)
      }
    )
  }

}
