import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpComponent } from './http.component';
import { CallApiComponent } from './call-api/call-api.component';

const routes: Routes = [
  {
    path:'',
    children:[
      {
        path:'callApi',
        component:CallApiComponent
      }
    ],
    component:HttpComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HttpRoutingModule { }
