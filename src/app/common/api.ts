import { environment } from "src/environments/environment";

function getUrl(endpoint: string) {
    // return environment.baseURL + '/api/' + endpoint
    return environment.baseURL + '/' + endpoint
}
export const API_URL = {
    getAvatarUrl : getUrl('photos'),
    getUsers     : getUrl('users'),
    getPost      : getUrl('posts')
}
