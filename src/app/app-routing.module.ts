import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'input', loadChildren:'./input/input.module#InputModule'},
  {path:'output', loadChildren:'./output/output.module#OutputModule'},
  {path:"pass", loadChildren:'./pass-data-service/pass-data-service.module#PassDataServiceModule'},
  {path:'http', loadChildren:'./http/http.module#HttpModule'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
